# Note: these tests are quite preliminary....
import numpy as np
import os
import sys
import unittest

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__))+"/../src/")  # hack...
from space_sensor_model import *


class GenericFOVSensorTest(unittest.TestCase):

    def test_Simple(self):

        coords = [0, 0, 10]
        coords_notVisible_Range = [0, 0, 11]
        coords_notVisible_Range2 = [1, 1, 10]
        coords_notVisible_yFOV = [0, 1.01, 1]
        coords_visible_yFOV = [0, 1, 1]

        coords_visible_xFOV = [6, 1, 1]
        coords_notVisible_xFOV = [16, 1, 1]

        sens = GenericFOVSensor()
        sens.max_range = 10
        sens.fov_x = np.pi*0.9
        sens.fov_y = np.pi/2

        # To lazy to put all these into seperate test functions...:
        assert(sens.is_visible(coords))
        assert(not sens.is_visible(coords_notVisible_Range2))
        assert(not sens.is_visible(coords_notVisible_Range))
        assert(not sens.is_visible(coords_notVisible_yFOV))
        assert(sens.is_visible(coords_visible_yFOV))
        assert(sens.is_visible(coords_visible_xFOV))
        assert(not sens.is_visible(coords_notVisible_xFOV))


if __name__ == '__main__':
    unittest.main()
