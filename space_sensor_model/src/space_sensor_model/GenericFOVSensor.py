# Sensor model for a generic sensor that calculates visibility
# based on:
#   Field of view (either horizontal/vertical or custom based on angles)
#   Range (either simple l2 range or custom function based on angles and distance)
# Note: Coordinates should already be in sensor frame!
import numpy as np
import transformations as tf

class GenericFOVSensor(object):

        def __init__(self):
            self.fov_x = 0
            self.fov_y = 0
            self.max_range = 0
            self.T_sensor_body = np.identity(4) #transformation from body frame to sensor frame

        def set_frame_transform(self, T):
            self.T_sensor_body = T

        def set_frame_by_string(self, str_q, str_p):
            T = GenericFOVSensor.get_transform_by_string(str_q, str_p)
            self.set_frame_transform(T)

        @staticmethod
        def get_transform_by_string(str_q, str_p):
            #takes a quaternion (q) from frame A to frame B and a position (p) in frame A
            # to calculate a transformation A => B
            # same semantics as tf strings
            q = np.array([float(x) for x in str_q.split(" ")])
            p = np.array([float(x) for x in str_p.split(" ")])

            # calculate transformation
            # pre-multiply rotation , as P is in frame A
            transform = np.dot(tf.quaternion_matrix([q[3], q[0], q[1], q[2]]).T, tf.translation_matrix(-p))

            return transform


        def to_sensor_frame(self, C_b):
            # test if 3 or 4 vector (homogenous)
            if C_b.size == 3:
                # make homogenous
                C_b = np.append(C_b, 1)

            # apply transform
            C_s = np.dot(self.T_sensor_body, C_b)

            # return non-homgenous 3-vector
            return C_s[0:3]


        def set_fov_range(self, fov_x, fov_y, max_range):
            self.fov_x = fov_x
            self.fov_y = fov_y
            self.max_range = max_range

        # To be implemented in subclass
        def get_measurement(self, coord):
            return 0

        def get_measurement_noise(self, coord):
            return 0

        def is_visible(self, coord):
            try:
                # returns error if measurement is behind sensor plane (=out of view anyway)
                angles = self.get_angles(coord)
            except:
                return False

            dist = np.linalg.norm(coord)
            return self.is_in_range(angles, dist) and self.is_in_fov(angles)

        def get_angles(self, coord):
            # Calculates angle of the observed Coordinates
            # w.r.t. sensor frame
            # x_ang = Angle between z-axis and x-axis, between -pi and pi
            if coord[2] <= 0:
                raise Exception("Measurement behind sensor not supported (coord_z<0)")

            angles = np.zeros(2)

            # angle between z-axis and coord in the x-z plane
            angles[0] = np.arctan2(coord[0], coord[2])


            # angle between z-axis and coord in the y-z plane
            angles[1] = np.arctan2(coord[1], coord[2])

            # handle wrap around
            if angles[0] > np.pi / 2.0:
                angles[0] = angles[0] - np.pi

            if angles[1] > np.pi / 2.0:
                angles[1] = angles[1] - np.pi

            return angles

        def is_in_range(self, angles, dist):
            return dist <= self.max_range

        def is_in_fov(self, angles):
            return abs(angles[0]) <= self.fov_x/2.0 and abs(angles[1]) <= self.fov_y/2.0
