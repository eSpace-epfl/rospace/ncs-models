import numpy as np
from GenericFOVSensor import GenericFOVSensor

# Sensor Model for sensor that measures Range
# with gaussian noise on the range measurement

class SimpleRangeFOVSensor(GenericFOVSensor):
    
    def __init__(self):
        super(SimpleRangeFOVSensor, self).__init__()
        self.mu = 0
        self.sigma = 1
    
    def set_measurement_noise(self, mu, sigma):
        self.mu = mu
        self.sigma = sigma

    def get_measurement(self, C_body):
        # get measurement in sensor frame
        C_s = self.to_sensor_frame(C_body)

        # check if visilbe
        is_visible = self.is_visible(C_s)

        if is_visible:
            noise_sample = self.get_measurement_noise(C_s)
            sensor_value = np.linalg.norm(C_s) + noise_sample
            return True, sensor_value

        else:
            return False, 0.0

    def get_measurement_noise(self, coord):
        return np.random.normal(self.mu, self.sigma)

# check quantities library

