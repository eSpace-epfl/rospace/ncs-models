import numpy as np
from GenericFOVSensor import GenericFOVSensor

# Sensor Model for sensor that measures bearing and azimuth relative to the sensor frame
# with gaussian noise on the range measurement

class SimpleAnglesFOVSensor(GenericFOVSensor):
    
    def __init__(self):
        super(SimpleAnglesFOVSensor, self).__init__()
        self.mu = 0
        self.sigma = 1
    
    def set_measurement_noise(self, mu, sigma):
        self.mu = mu
        self.sigma = sigma

    def get_measurement(self, C_body):
        # get measurement in sensor frame
        C_s = self.to_sensor_frame(C_body)

        # check if visilbe
        is_visible = self.is_visible(C_s)

        if is_visible:
            # don't add noise at the moment
            sensor_value = self.get_angles(C_s)
            return True, sensor_value

        else:
            return False, np.array([0.0, 0.0])


    def get_measurement_noise(self, coord):
        return np.array([0,0])

# check quantities library

