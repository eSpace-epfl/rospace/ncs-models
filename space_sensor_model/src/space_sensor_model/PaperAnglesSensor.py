import numpy as np
from GenericFOVSensor import GenericFOVSensor


# Sensor Model according to the papers
# by Sullivan / D'Amico for reproductibility purposes
class PaperAnglesSensor(GenericFOVSensor):

    def __init__(self):
        super(PaperAnglesSensor, self).__init__()
        self.mu = 0
        self.sigma = 1

    def set_measurement_noise(self, mu, sigma):
        self.mu = mu
        self.sigma = sigma

    def get_measurement(self, C_body):
        # get measurement in sensor frame
        C_s = self.to_sensor_frame(C_body)

        # at the moment, it is always visible (for testing)
        is_visible = True

        angles = np.zeros(2)

        angles[0] = np.arcsin(C_s[1] / np.linalg.norm(C_s))

        # angle between z-axis and coord in the x-z plane
        angles[1] = np.arctan(C_s[0] / C_s[2])  # p.arctan2(coord[0], coord[2])

        return is_visible, angles

    def get_measurement_noise(self, coord):
        return np.array([0, 0])

