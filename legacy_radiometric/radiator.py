# -*- coding: utf-8 -*-

"""
Module implementing radiometric source

author: Christophe Paccolat
"""

import abc

from scipy import constants as cst
from scipy.stats import norm

from .baseobject import BaseObject
from .tools import *


class Radiator(BaseObject):
    __metaclass__ = abc.ABCMeta

    def __init__(self, r0, n0):
        super().__init__(r0, n0)

    def radiance_sp(self, wl: float,
                    observer: BaseObject,
                    source: BaseObject) -> float:
        """Calculate the spectral radiance normal to the
        surface of the observer (W/m**2*sr*nm)"""
        pass

    def domega_psi(self, observer: BaseObject) -> float:
        """Returns the solid viewing angle from an observers perspective"""
        pass


class BlackBody(Radiator):
    def __init__(self, r0: np.ndarray, temp: float, radius: float):
        super().__init__(r0, np.array([0, 0, 1]))
        self.temperature = temp  # Surface temperature
        self.radius = radius  # Cross section

    @staticmethod
    def B_lambda(wl: float, temp: float) -> float:
        """ Calculate the spectral radiance an object from Planck's law

        :param wl: wave length
        :param temp: black body temperature
        :return: spectral radiance
        """

        h = cst.h  # Planck's constant (in J*s)
        c = cst.c  # Speed of light (in m/s)
        k_b = cst.Boltzmann  # Boltzmann constant (in J/K)

        B = 2 * h * c ** 2 / wl ** 5 / (exp((h * c / (wl * k_b * temp))) - 1)
        return B

    def radiance_sp(self, wl: float,
                    observer: BaseObject = None,
                    source: Radiator = None) -> float:
        return self.B_lambda(wl, self.temperature)

    def domega_psi(self, observer: BaseObject) -> float:
        distance = np.linalg.norm(self.r0 - observer.r0)
        return pi * self.radius ** 2 / distance ** 2


class Beam(Radiator):
    """ Model a gaussian beam type of radiator"""
    def __init__(self,
                 r0: np.ndarray,
                 n0: np.ndarray,
                 fieldofregard: float,
                 power: float,
                 peak_wl: float,
                 bw: float = None):
        super().__init__(r0, n0)
        self.FOR = fieldofregard  # Field of Regard (in rad)
        self.nominal_power = power  # Emission power (in W)
        self.peak_wl = peak_wl  # Emission wavelength (in m)
        self.bw = bw  # Source bandwidth (full width at half max) (in m)

    @property
    def FOR(self):
        return self._FOR

    @FOR.setter
    def FOR(self, value: float) -> None:
        self._FOR = value
        self._w0 = 2*self.peak_wl/(pi*value)  # waist radius of the beam

    def radiant_flux_sp(self, wl: float) -> float:
        """Return the spectral flux (in [W/m]) based on specified properties"""
        mean = self.peak_wl
        if self.bw is None:
            sigma = 0.05 * self.peak_wl
        else:
            sigma = self.bw / (2 * sqrt(2 * log(2)))  # By definition of FWHM

        return self.nominal_power * norm.pdf(wl, mean, sigma)

    def radiance_sp(self, wl: float,
                    observer: BaseObject = None,
                    source: Radiator = None) -> float:
        """ In case of a beam, we can not calculate the beam's radiance accurately
        since it is assumed punctual. Instead we return the radiant intensity [W/sr].
        Alternatively one could use the beam's waist to define radiance. """

        zp = np.linalg.norm(observer.r0 - self.r0)  #
        theta = angle(observer.r0 - self.r0, self.n0)  # Deviation with respect to the optical axis
        w0_2 = self._w0 ** 2
        I0 = 2 * self.radiant_flux_sp(wl) / (pi * w0_2)
        zr_2 = (pi * w0_2 / wl)**2  # Rayleigh range of the beam
        z_2 = (zp * cos(theta))**2
        k = z_2 / (1 + z_2/zr_2)
        return k * I0 * exp(-2 * tan(theta) * k / w0_2)

    def domega_psi(self, observer: BaseObject) -> float:
        """ Return a 1/r**2 value to remain coherent with the punctual source
        assumption. """
        return np.linalg.norm(observer.r0 - self.r0) ** -2


class Lamp(Radiator):
    """ Model a gaussian beam type of radiator"""
    def __init__(self,
                 r0: np.ndarray,
                 n0: np.ndarray,
                 spectrum,
                 FOR):
        super().__init__(r0, n0)
        self.spectrum = spectrum
        self.FOR = FOR

    @property
    def FOR(self):
        return self._FOR

    @FOR.setter
    def FOR(self, value: float) -> None:
        self._FOR = value

    def radiance_sp(self, wl: float,
                    observer: BaseObject = None,
                    source: Radiator = None) -> float:
        return self.spectrum(wl)  # Given in W/m**2 at 1m => == W/sr

    def domega_psi(self, observer: BaseObject) -> float:
        """ Return a 1/r**2 value to remain coherent with the punctual source
        assumption. """
        return np.linalg.norm(observer.r0 - self.r0) ** -2
