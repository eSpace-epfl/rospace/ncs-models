# -*- coding: utf-8 -*-

"""
Module implementing radiometric reflector

author: Christophe Paccolat
"""

from scipy.special import erfc, lambertw

from .radiator import *
from .camera import Camera
from PyTMM.refractiveIndex import *
from PyTMM.transferMatrix import *
from .tools import *
import numpy as np


def local_tr_mx(z_hat: np.ndarray,
                ref_x: np.ndarray):
    """Return the transformation matrix such that the normal of the
    surface is [0,0,1]. The projection of ref_x onto the surface gives x_hat"""
    x_hat = normalized(ref_x - np.dot(ref_x, z_hat) * z_hat)  # make x_hat perp. with z_hat
    y_hat = np.cross(z_hat, x_hat)

    return np.vstack((x_hat, y_hat, z_hat))


def htsg_sum(g, T):
    """The infinite sum is evaluated directly for g < 15 (40 terms are used);
    otherwise, an analytic approximation is used, based on rewriting the sum
    (using the Sterling formula to approximate the factorial) as sum= exp(f(m)).
    The maximum of f(m) is found, and f is rewritten as a truncated Taylor
    series around the maximum. The resulting sum is then approximated by
    an integral of the resulting Gaussian. """

    if g < 15:
        ssum = 0.0
        term1 = exp(-g)
        for m in range(1, 40):
            recm = 1.0/m
            term1 *= g*recm
            ssum += term1*recm*exp(T*recm)
        return ssum
    else:
        mx = g
        for _ in range(0, 4):
            # FIXME: exp() overflows when T > 705.
            mx = g*np.exp(-1.5/mx - T / mx**2)
        return sqrt(g)*exp(mx*log(g)-g-(mx+1.5)*log(mx)+mx+T/mx)


def getComplexRefraction(material: Material, wl: float) -> complex:
        try:
            return material.getRefractiveIndex(wl*1e9)\
                   + material.getExtinctionCoefficient(wl*1e9)*1j
        except NoExtinctionCoefficient:
            return material.getRefractiveIndex(wl*1e9)


class Surface(Radiator):
    def __init__(self,
                 r0: np.ndarray,
                 n0: np.ndarray,
                 roughness: float,
                 autocorrelation: float,
                 layers: list,
                 environment: Material,
                 dims: np.ndarray):

        super().__init__(r0, n0)

        self.sigma0 = roughness
        self.tau = autocorrelation

        self.layers = layers
        self.environment = environment

        self.dims = dims

    @property
    def layers(self) -> list:
        return self._layers

    @layers.setter
    def layers(self, layers: list):
        """Set the material of the surface and adapt refraction function"""
        self._layers = layers

    @property
    def environment(self) -> Material:
        return self._environment

    @environment.setter
    def environment(self, environment: Material):
        """Set the environment of the surface and adapt refraction function"""
        self._environment = environment

    def radiance_sp(self, wl: float,
                    observer: Camera,
                    source: Radiator) -> float:
        """Calculate the contribution (dL_i) of the source to the
        spectral radiance radiance_sp of the surface in direction of the observer"""

        domega_psi = source.domega_psi(observer)
        radiance = source.radiance_sp(wl, observer=self)
        theta_i = angle(source.r0 - self.r0, self.n0)
        bdrf = self.bdrf(wl, observer, source, domega_psi)

        return bdrf * radiance * cos(theta_i) * domega_psi

    def bdrf(self, wl: float,
             observer: Camera,
             source: Radiator,
             domega_psi: float) -> float:

        k_i = (self.r0 - source.r0)
        P = local_tr_mx(self.n0, -k_i)  # n_0 = \hat{z}, perp cmp of -k_i to n_0 is \hat{x}.
        k_i = normalized(P @ k_i)
        k_r = -normalized(P @ observer.n)
        n0 = np.array([0., 0., 1.])

        eps = 0.0001
        k_i[np.abs(k_i) < eps] = 0
        k_r[np.abs(k_r) < eps] = 0

        v = k_r - k_i  # Halfway vector

        theta_i = angle(-k_i, n0)
        theta_r = angle(k_r, n0)

        F = self.F(wl, angle(-k_i, v))
        S = self.S(theta_i) * self.S(theta_r)
        g = self.g(wl, theta_i, theta_r)

        """Calculate specular reflection term (eq. 70)"""
        if type(source) is BlackBody:
            # Check whether observer is within the specular clone, ie.
            # the reflection of the solid angle subtending the source.
            alpha = acos(1 - domega_psi / (2 * pi))  # half angle of the cone intercepting the source
            delta = self.delta(-k_i, k_r, alpha)
        elif type(source) is Beam or type(source) is Lamp:
            # Check whether observer is within portion of FOR reflected
            # by surface
            d = np.linalg.norm(source.r0 - self.r0)
            red_for = max(atan2(self.dims.min()*cos(theta_i), d), source.FOR)

            # Position of virtual (reflected) position of the source
            ref_src_r0 = source.r0 - 2*np.dot((source.r0 - self.r0), n0)*n0

            vec = observer.r0 - ref_src_r0
            delta = self.delta(-source.n0, vec, red_for)
        else:
            raise NotImplementedError("Unknown source type")

        rho_s = F * exp(-g) * S
        rho_sp = rho_s / (cos(theta_i) * domega_psi) * delta

        """Calculate the directional diffuse term (eq. 71)
        The function is adapted to avoid a division by zero."""
        G = self.G(k_i, k_r)
        D = self.D(wl, g, v)

        rho_dd = F / pi \
                 * G * S * D / (cos(theta_r) * cos(theta_i))

        """Calculate the uniform diffuse term (eq. 72)"""
        rho = 0.00  # Surface albedo TODO: define based on material
        rho_ud = rho / pi

        return rho_sp + rho_dd + rho_ud

    def F(self, wl: float, theta_i: float) -> float:
        """Calculate the Fresnel reflection (eq. 75)
        note: Polarisation of light is ignored

        :param wl: Wavelength of incoming radiation
        :param theta_i: incidence angle wrt normal

        :return: Fraction of reflected radiation
        """

        n_env = getComplexRefraction(self.environment, wl)

        if len(self.layers) == 1:
            n_mat = getComplexRefraction(self.layers[0]['material'], wl)
            # a_p = TransferMatrix.boundingLayer(n_env, n_mat, theta_i, Polarization.p)
            # a_s = TransferMatrix.boundingLayer(n_env, n_mat, theta_i, Polarization.s)
            # R_p, T_p = solvePropagation(a_p)
            # R_s, T_s = solvePropagation(a_s)
            # return (R_p ** 2 + R_s ** 2) / 2

            cos_t = np.cos(theta_i)
            root = np.sqrt(1 - (n_env / n_mat * np.sin(theta_i)) ** 2)

            # Beyond the critical angle, reflection is total
            if np.isnan(root):
                return 1.0

            Fs = np.abs((n_env * cos_t - n_mat * root) / (n_env * cos_t + n_mat * root)) ** 2
            Fp = np.abs((n_env * root - n_mat * cos_t) / (n_env * root + n_mat * cos_t)) ** 2

            return (Fs + Fp) / 2.0

        else:
            n_mat = [getComplexRefraction(layer['material'], wl) for layer in self.layers]
            a_p = TransferMatrix.boundingLayer(n_mat[1], n_mat[0], theta_i, Polarization.p)  # Base layer
            a_s = TransferMatrix.boundingLayer(n_mat[1], n_mat[0], theta_i, Polarization.s)  # Base layer

            for layer in self.layers[1:]:
                d = layer['thickness']
                n = n_mat[self.layers.index(layer)]
                b_p = TransferMatrix.propagationLayer(n, d, wl, theta_i, Polarization.p)
                b_s = TransferMatrix.propagationLayer(n, d, wl, theta_i, Polarization.s)
                a_p.appendRight(b_p)
                a_s.appendRight(b_s)

            env_p = TransferMatrix.boundingLayer(n_env, n_mat[-1], theta_i, Polarization.p)
            env_s = TransferMatrix.boundingLayer(n_env, n_mat[-1], theta_i, Polarization.s)
            a_p.appendRight(env_p)
            a_s.appendRight(env_s)

            R_p, T_p = solvePropagation(a_p)
            R_s, T_s = solvePropagation(a_s)

            return (R_p ** 2 + R_s ** 2) / 2

    def g(self,
          wl: float,
          theta_i: float,
          theta_r: float) -> float:
        """Calculate the roughness factor (eq. 79)

        (Harvey et al. 2007): It corresponds to the phase variation introduced by an RMS
        surface roughness factor sigma.
        """

        tau_2sigma = self.tau / (2 * self.sigma0)
        Kr = tan(theta_r) * erfc(tau_2sigma * cot(theta_r))  # eq. 83
        Ki = tan(theta_i) * erfc(tau_2sigma * cot(theta_i))  # eq. 82

        # The root of eq. 83 is obtained by solving Lambert's function, giving
        # z0 = +-sigma0 * sqrt(W(a**2)). Further since a is real positive
        # we know that the solution is on the k=0 branch of the function
        a = (Ki + Kr) / (2.0 * sqrt(2.0 * pi))
        W = lambertw(a ** 2, k=0).real
        z0 = self.sigma0 * sqrt(W)

        sigma = self.sigma0 / sqrt(1 + (z0 / self.sigma0) ** 2)  # eq.

        return (2 * pi * sigma / wl * (cos(theta_i) + cos(theta_r))) ** 2  # eq. 79

    def S(self, theta: float) -> float:
        """Calculate partial shadowing function (eq. 24)"""

        # Avoid a division by zero.
        if theta == 0.0:
            return 1.0

        tct_sigma = self.tau * cot(theta) / self.sigma0  # tau*cot theta / sigma
        L = (2.0 / sqrt(pi)
             / tct_sigma
             - erfc(tct_sigma / 2)) / 2  # eq. 25
        return (1
                - erfc(tct_sigma / 2) / 2) \
               / (L + 1)

    def delta(self, axis: np.ndarray,
              vec: np.ndarray, half_angle: float) -> bool:
        """Return whether we are in the specular cone (eq. 74)"""
        # if point source, calculate if source in cone whose axis is mirror vector of
        # axis by n0
        n = np.array([0, 0, 1])
        axis_ref = 2 * np.dot(axis, n) * n - axis  # Reflected vector
        if angle(axis_ref, vec) > half_angle:
            return False
        else:
            return True

    def D(self,
          wl: float,
          g: float,
          v: np.ndarray) -> float:
        """Distribution function for the directional diffuse reflection term (eq. 78)

        This quantity is the time average of the squared modulus of the electric field
        vector, also called mean scattered.

        (Cook et Torrance, 1981): Fraction of the facets that are oriented in the
        direction of (k_i+k_r)/2
        """
        vxy2 = v[0] ** 2 + v[1] ** 2  # (eq. 84b)
        fact = (pi * self.tau / wl) ** 2

        s = htsg_sum(g, -vxy2 * fact)

        return fact / 4 * s

    def G(self, k_i: np.ndarray, k_r: np.ndarray):
        """Calculate the geometrical attenuation factor (eq. 76)

        (Cook et Torrance, 1981): Accounts for shadowing of one facet by
        another."""
        v = k_r - k_i
        n = np.array([0, 0, 1])
        v_z = np.dot(k_r, n) / np.linalg.norm(k_r)\
              + np.dot(-k_i, n) / np.linalg.norm(k_i)
        s_i = normalized(np.cross(k_i, n))
        p_i = np.cross(s_i, k_i)  # eq. 85
        s_r = normalized(np.cross(k_r, n))
        p_r = np.cross(s_r, k_r)  # eq. 86

        return (np.dot(v, v) / v_z) ** 2 \
               * (np.dot(s_r, k_i) ** 2 + np.dot(p_r, k_i) ** 2) \
               * (np.dot(s_i, k_r) ** 2 + np.dot(p_i, k_r) ** 2) \
               / np.linalg.norm(np.cross(k_i, k_r)) ** 4
