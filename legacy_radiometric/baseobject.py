# -*- coding: utf-8 -*-

"""
Module containing an abstract class defining only
an object with a position. Base for radiator,
surface and camera classes

author: Christophe Paccolat
"""

import abc
from .tools import *


class BaseObject:
    __metaclass__ = abc.ABCMeta

    def __init__(self, r0: np.ndarray, n0: np.ndarray):
        self.r0 = r0
        self.n0 = n0

    @property
    def r0(self) -> np.ndarray:
        return self._r0

    @r0.setter
    def r0(self, value: np.ndarray):
        s = value.shape
        if s == (3,):
            self._r0 = value
        else:
            raise TypeError("Array shape given: " + s.__str__() + ", expected (3,)")

    @property
    def n0(self) -> np.ndarray:
        return self._n0

    @n0.setter
    def n0(self, value: np.ndarray):
        s = value.shape
        if s == (3,):
            self._n0 = normalized(value)
        else:
            raise TypeError("Array shape given: " + s.__str__() + ", expected (3,)")
