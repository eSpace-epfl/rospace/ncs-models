# -*- coding: utf-8 -*-

"""
Module containing several useful functions

author: Christophe Paccolat
"""

import numpy as np
from math import *


def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians using the Euler-Rodrigues forumla.
    """
    axis = np.asarray(axis, dtype=np.float)
    axis /= np.linalg.norm(axis)
    a = np.cos(theta/2.0)
    b, c, d = -axis*sin(theta/2.0)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])


def cot(theta: float) -> float:
    """Cotangent function"""
    return tan(theta) ** -1


def normalized(a, axis=-1, order=2):
    """Returns the unit/normalized vector of the input."""
    a = np.array(a)
    if np.linalg.norm(a, order, axis) == 0:
        raise ValueError("Can't normalize a zero vector")
    l2 = np.atleast_1d(np.linalg.norm(a, order, axis))
    l2[l2 == 0] = 1
    if a.ndim == 1:
        return a / l2
    return a / np.expand_dims(l2, axis)


def angle(v1: np.ndarray, v2: np.ndarray) -> float:
    """Returns the angle (positive) between two vectors"""
    v1u = normalized(v1)
    v2u = normalized(v2)
    return acos(np.clip(np.dot(v1u, v2u), -1.0, 1.0))