# -*- coding: utf-8 -*-

"""
Module implementing a complete camera,
including sensor and optics

author: Christophe Paccolat
"""

from warnings import warn

import scipy.constants as cst
from scipy.integrate import quad
from scipy.interpolate import interp1d

from .baseobject import BaseObject
from .radiator import Radiator
from .tools import *


class Sensor:
    # DN = pq.UnitQuantity('Analog-to-Digital Units', symbol='ADU')
    arg_peak_QE = 0

    def __init__(self,
                 manufacturer: str,
                 name: str,
                 pitch: np.ndarray,
                 resolution: np.ndarray,
                 spectral_range: np.ndarray,
                 full_well: int,
                 read_noise: float,
                 dark_current: float,
                 quantum_efficiency: np.ndarray):
        self.manufacturer = manufacturer
        self.name = name
        self.pitch = pitch
        self.res = resolution
        self.srange = spectral_range
        self.Q_sat = full_well
        self.sigma_read = read_noise
        self.i_dc = dark_current
        self.QE = quantum_efficiency

    def Q(self,
          i: float,
          t_int: float,
          saturate=True) -> float:
        q = cst.elementary_charge
        out = i * t_int / q
        if out > self.Q_sat and saturate:
            return self.Q_sat
        else:
            return out

    def mu_DN(self,
              i_ph: float,
              t_int: float,
              gain: float):
        q = cst.elementary_charge
        return gain * self.Q(i_ph + self.i_dc*q, t_int)

    def snr(self,
            i_ph: float,
            t_int: float,
            gain: int) -> float:
        q = cst.elementary_charge
        s_r = self.sigma_read
        i_dc = self.i_dc
        s_s = (i_dc + i_ph) * t_int / q
        s_q = 1.0 / 12.0
        return i_ph * t_int / (q * sqrt(s_r ** 2 + s_s ** 2 + (s_q / gain) ** 2))

    def snr_db(self,
               i_ph: float,
               t_int: float,
               gain: int) -> float:
        return 10 * log10(self.snr(i_ph, t_int, gain))

    @property
    def QE(self):
        """Returns the quantum efficiency function"""
        return self._QE

    @QE.setter
    def QE(self, quantum_efficiency_db):
        """Interpolate the quantum efficiency function based discreet value."""

        x_low = self.srange.min()
        x_high = self.srange.max()
        steps = quantum_efficiency_db.size
        x = np.linspace(x_low, x_high, steps, endpoint=True)
        y = quantum_efficiency_db
        max_arg = quantum_efficiency_db.argmax()
        self.arg_peak_QE = x_low + max_arg * (x_high - x_low) / (steps - 1)
        self._QE = interp1d(x, y, fill_value='extrapolate')


class Lens(BaseObject):
    def __init__(self,
                 aperture_range: np.ndarray = None,
                 f_range: np.ndarray = None,
                 transmittance: callable([[float], float]) = lambda wl: 1,
                 r0: np.ndarray = np.array([0, 0, 0]),
                 n0: np.ndarray = np.array([0, 1, 0])):
        super().__init__(r0, n0)
        if aperture_range is None:
            aperture_range = np.array([10e-3, 10e-3])
        if f_range is None:
            f_range = np.array([12e-3, 12e-3])
        self.aperture_range = aperture_range
        self.focal_length_range = f_range
        self.transmittance = transmittance
        self.fillfactor = 1.0  # Set to one for now (micro-lenses)
        self.f = f_range[0]
        self.aperture = aperture_range[1]
        self.r0 = r0
        self.n0 = n0

    @property
    def aperture(self) -> float:
        return self._aperture

    @aperture.setter
    def aperture(self, value: float):
        if value > self.aperture_range.max():
            warn("Aperture is higher than admissible range")
        if value < self.aperture_range.min():
            warn("Aperture is lower than admissible range")
        self._aperture = value

    @property
    def f(self) -> float:
        return self._f

    @f.setter
    def f(self, value: float):
        if value > np.max(self.focal_length_range):
            warn("Focal length is higher than admissible range")
        if value < np.min(self.focal_length_range):
            warn("Focal length is lower than admissible range")
        self._f = value


class Camera(BaseObject):
    def __init__(self, sensor: Sensor, lens: Lens = None,
                 r0: np.ndarray = np.array([0, 0, 0]),
                 n0: np.ndarray = np.array([0, 1, 0]),
                 py: np.ndarray = np.array([0, 0, 1])):
        super(Camera, self).__init__(r0, n0)
        if lens is None:
            lens = Lens()
        self.lens = lens
        self.sensor = sensor
        self.active_pixel = np.zeros([2, ])  # Default pixel is centered on line of sight.
        self.py = py  # Direction of the -y direction on the sensor

    @property
    def n(self) -> np.ndarray:
        """ Return the line of sight modified to consider the active pixel"""
        dev_angle = np.arctan2(self.sensor.pitch*self.active_pixel, self.lens.f)
        py = self.py
        n0 = self.n0
        px = np.cross(py, n0)
        return np.dot(rotation_matrix(py, dev_angle[0]), np.dot(rotation_matrix(px, dev_angle[1]), n0))

    @property
    def ifov(self) -> float:
        return 2*atan2(self.sensor.pitch.mean(), 2*self.lens.f)

    @property
    def fov(self) -> np.ndarray:
        gamma = self.sensor.pitch
        f = self.lens.f
        res = self.sensor.res
        return 2*np.arctan2(gamma * res, 2*f)

    def i_ph(self, src: Radiator, primary_src: Radiator = None,
             pix_position: np.ndarray = None, has_atmosphere: bool = True) -> float:
        if pix_position != None:
            self.active_pixel = pix_position
        d = self.lens.aperture
        G = self.sensor.pitch
        f = self.lens.f
        h = cst.h  # Planck's constant
        c = cst.c  # Speed of light
        alpha = atan2(np.linalg.norm(self.active_pixel)*G, f)  # incident light angle with sensor normal
        Sigma = pi / 4 * (d / f) ** 2 * cos(alpha) ** 4  # Solid angle observed by pixel
        g = self.lens.fillfactor * G ** 2
        #TODO: account for jitter
        pixel_area = (self.ifov * np.linalg.norm(self.r0-src.r0))**2
        src_srf = (2*80e-3*40e-3)

        Spread = src_srf / pixel_area

        limits = self.sensor.srange
        if has_atmosphere:
            atm_transmission = atm.get_atm_data(limits)
        else:
            atm_transmission = lambda wl: 1.0

        integral = quad(lambda wl: src.radiance_sp(wl, self, primary_src)
                                   * self.lens.transmittance(wl) * atm_transmission(wl)
                                   * self.sensor.QE(wl) * wl / (h * c) * cst.elementary_charge,
                        limits[0], limits[1], limit=500)[0]

        return g * Sigma * integral * Spread
